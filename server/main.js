/**
 * Created by LENOVO on 10/21/2016.
 */
const express = require("express");
const path = require("path");

//Create an instance of the express application
const app = express();

const docroot = path.join(__dirname, "..");

app.set("port", process.argv[2] || process.env.APP_PORT || 3000);

// Serve files from public directory.
// __dirname is the absolute path of the application directory
app.use(express.static(path.join(docroot, "public")));
app.use(express.static(path.join(docroot, "public/bower_components")));
// app.use(express.static(__dirname + "/../client"));

app.use(function(req, res){
    res.redirect("error404.html");
});
//Start the web server on port 3000
app.listen(app.get("port"), function() {
    console.info("Web Server started on port %d", app.get("port"));
    console.info("__dirname: " + __dirname);
    console.info("docroot: " + docroot);
    console.info(path.join(docroot, "client/bower_components"));
});
