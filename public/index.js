/**
 * Created by LeeChuan on 19/10/16.
 */

(function(){
    var RegApp = angular.module("RegApp", []);
    var RegCtrl = function($http) {
        var regCtrl = this;

        regCtrl.form = {
            email: "Youremail@domain.com",
            password: "",
            name: "Your name",
            gender: "Unkown",
            bod: "",
            address: "",
            country: "",
            contact: ""
        };

        regCtrl.age = 20;

        regCtrl.bodValidation = function() {
            console.info("BOD validation");
        };

        regCtrl.resetEntry = function() {
            console.info("Reset Entry");
            regCtrl.form.email = "";
            regCtrl.form.password = "";
            regCtrl.form.name = "";
            regCtrl.form.gender = "";
            regCtrl.form.bod = "";
            regCtrl.form.address = "";
            regCtrl.form.country = "";
            regCtrl.form.contact = "";
        };

        regCtrl.sendSuccess = false;

        regCtrl.submit = function() {
                $http
                    .post("thankyou.html",regCtrl.form)
                    //.post("/api/users/123456789?test=true&mock=true",{firstName: "Jon", lastName: "Snow"})
                    .then(function (response) {

                        // if you want to redirect
                        //window.location.href = response.data.redirectUrl;

                        // Display message on the same page
                        regCtrl.sendSuccess = true;

                        console.log("Form Submitted");
                    })
                    .catch(function (response) {
                        // if you want to redirect
                        //window.location.href = response.data.redirectUrl;

                        // Display message on the same page
                        regCtrl.sendSuccess = false;

                        console.log("Form submission failed");
                    });
            regCtrl.sendSuccess = true;  // for test only
            if (regCtrl.sendSuccess){
                $http
                    .get("thankyou.html")
            }
//            console.info("Submit");
//            console.info(regCtrl.form);

        };

        // birthday is a date
        regCtrl.bodValidation = function (){
            var ageDifMs = Date.now() - regCtrl.form.bod.getTime();
            var ageDate = new Date(ageDifMs); // miliseconds from epoch
            regCtrl.age = Math.abs(ageDate.getUTCFullYear() - 1970);
            console.info(regCtrl.age);
            console.info(regCtrl.form.bod);
        }

    };

    RegApp.controller("RegCtrl", ["$http", RegCtrl]);

})();