/**
 * Created by LENOVO on 10/21/2016.
 */
(function() {

    var RegApp = angular.module("RegApp", []);

    var RegCtrl = function() {
        var regCtrl = this;

        regCtrl.form = {
            email: "Your email",
            password: "",
            name: "Your name",
            gender: "Unkown",
            bod: "",
            address: "",
            country: "",
            contact: ""
        };

        regCtrl.isValidAge = false;

        regCtrl.bodValidation = function() {
            console.info("BOD validation");
        };

        regCtrl.resetEntry = function() {
            console.info("Reset Entry");
            regCtrl.email = "";
            regCtrl.password = "";
            regCtrl.name = "";
            regCtrl.gender = "";
            regCtrl.bod = "";
            regCtrl.address = "";
            regCtrl.country = "";
            regCtrl.contact = "";
        };

        regCtrl.submit = function() {
            console.info("Submit");
            console.info(regCtrl.form);

        };
    };

    RegApp.controller("RegCtrl", [ RegCtrl ]);

})();
